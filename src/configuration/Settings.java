package configuration;

public class Settings {

  private int numberThreads = 8;
  private boolean isMd5Enabled;
  private boolean isShaEnabled;

  private static Settings settings = new Settings();

  private Settings() {

  }

  public static Settings getInstance() {
    return settings;
  }


  public int getNumberThreads() {
    return numberThreads;
  }

  public void setNumberThreads(int numberThreads) {
    this.numberThreads = numberThreads;
  }

  public boolean isMd5Enabled() {
    return isMd5Enabled;
  }

  public void setMd5Enabled(boolean md5Enabled) {
    isMd5Enabled = md5Enabled;
  }

  public boolean isShaEnabled() {
    return isShaEnabled;
  }

  public void setShaEnabled(boolean shaEnabled) {
    isShaEnabled = shaEnabled;
  }
}
