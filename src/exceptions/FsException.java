package exceptions;

public class FsException extends  RuntimeException{

  public FsException() {
  }

  public FsException(String message) {
    super(message);
  }

  public FsException(Throwable cause) {
    super(cause);
  }
}
