package fssystem;

import exceptions.FsException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ChecksumCalculator {

  public static final String SHA = "SHA-1";
  public static final String MD5 = "MD5";


  public String getHashCode(File file, String algorithm) {
    return algorithm == null || algorithm.isEmpty() ? getHash(file, MD5) : getHash(file, algorithm);
  }


  private String getHash(File file, String algorithm) {
    try {
      return getFileChecksum(file, algorithm);
    } catch (IOException | NoSuchAlgorithmException e) {
      throw new FsException(e);
    }
  }

  private static String getFileChecksum(File file, String algorithm)
      throws IOException, NoSuchAlgorithmException {
    //Get file input stream for reading the file content
    MessageDigest digest = MessageDigest.getInstance(algorithm);
    FileInputStream fis = new FileInputStream(file);

    //Create byte array to read data in chunks
    byte[] byteArray = new byte[1024];
    int bytesCount = 0;

    //Read file data and update in message digest
    while ((bytesCount = fis.read(byteArray)) != -1) {
      digest.update(byteArray, 0, bytesCount);
    }

    //close the stream; We don't need it now.
    fis.close();

    //Get the hash's bytes
    byte[] bytes = digest.digest();

    //This bytes[] has bytes in decimal format;
    //Convert it to hexadecimal format
    StringBuilder sb = new StringBuilder();
    for (byte aByte : bytes) {
      sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
    }

    //return complete hash
    return sb.toString();
  }
}
