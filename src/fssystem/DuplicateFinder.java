package fssystem;

import configuration.Settings;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DuplicateFinder {


  private ExecutorService es;
  private FileScanner fScanner = new FileScanner();

  public List<GroupLocation> findCopies(List<String> folders) {
    return findDuplicates(fScanner.findAllFiles(folders));
  }

  private List<GroupLocation> findDuplicates(List<File> allFiles) {
    final List<GroupLocation> result = Collections.synchronizedList(new ArrayList<GroupLocation>());
    es = Executors.newFixedThreadPool(Settings.getInstance().getNumberThreads());
    es.submit(() -> {
      //hash - path
      Map<String, GroupLocation> hashes = new ConcurrentHashMap<>();
      ChecksumCalculator checksumCalculator = new ChecksumCalculator();

      for (File file : allFiles) {

      }

    });
    es.shutdown();
    try {
      es.awaitTermination(365, TimeUnit.DAYS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return result;
  }


  private String getHash(File file, ChecksumCalculator checksumCalculator) {
    boolean isMd5 = Settings.getInstance().isMd5Enabled();
    boolean isSha = Settings.getInstance().isShaEnabled();
    String result = "";
    if ((!isMd5 && !isSha) || (isMd5)) {
      result = checksumCalculator.getHashCode(file, ChecksumCalculator.MD5);
    }
    if (isSha) {
      result += checksumCalculator.getHashCode(file, ChecksumCalculator.SHA);
    }

    return result;
  }
}
