package fssystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class FileScanner {

  List<File> findAllFiles(List<String> folders) {
    List<File> result = new ArrayList<>();
    for (String folder : folders) result.addAll(findAllFiles(folder));
    return result;
  }

  private List<File> findAllFiles(String folder) {
    List<File> result = new ArrayList<>();
    if (folder == null || folder.isEmpty()) return result;
    File currentFolder = new File(folder);
    if (!currentFolder.exists() || !currentFolder.canRead()) return result;
    if (currentFolder.isFile()) {
      result.add(currentFolder);
      return result;
    }
    File[] internalFiles = currentFolder.listFiles();
    if (internalFiles == null || internalFiles.length == 0) return result;
    for (File file : internalFiles) {
        if(file == null || !file.exists() || !file.canRead() || !file.canWrite()) continue;
        if(file.isDirectory()) result.addAll(findAllFiles(file.getAbsolutePath()));
        else result.add(file);
    }
    return result;
  }


}
